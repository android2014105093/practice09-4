package video.ex.vr.ex_video;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Toast;

import java.io.IOException;
import java.util.prefs.Preferences;

public class Act1 extends AppCompatActivity {

    private MediaPlayer mPlayer = null;
    private SurfaceView mView;
    private SurfaceHolder mHolder;
    private String mSdPath = Environment.getExternalStorageDirectory ()
            .getAbsolutePath();
    private String mVideoFile = "video.mp4";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act1);

        askForPermission(Manifest.permission.READ_EXTERNAL_STORAGE);

        mView = (SurfaceView) findViewById(R.id.surfaceView1);
        mHolder = mView.getHolder();
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS );
        mPlayer = new MediaPlayer();
    }

    private void askForPermission(String permission)
    {
        if(ContextCompat.checkSelfPermission(Act1.this, permission) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(Act1.this, permission)) {
                ActivityCompat.requestPermissions(Act1.this, new String[]{permission}, 2);
            } else {
                ActivityCompat.requestPermissions(Act1.this, new String[]{permission}, 2);
            }
        } else {
            Toast.makeText(this, "" + permission + " is already granted", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(ActivityCompat.checkSelfPermission(this, permissions[0]) == PackageManager.PERMISSION_GRANTED) {
            Intent imageIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(imageIntent, 2);
            Toast.makeText(this, "Permission granted", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show();
        }
    }

    protected void onDestroy() {
        if (mPlayer != null) {
            mPlayer.release();
            mPlayer = null;
        }
        super.onDestroy();
    }

    public void startMP(View v) {
        try {
            System.out.println(mSdPath);
            mPlayer.setDataSource(mSdPath + "/" + mVideoFile);
            mPlayer.setDisplay(mHolder);
            mPlayer.prepare();
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
        mPlayer.setLooping(true);
        mPlayer.start();
    }

    public void stopMP(View v) {
        mPlayer.stop();
        mPlayer.reset();
    }
}
